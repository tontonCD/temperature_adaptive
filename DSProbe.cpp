/* file: DSProbe.cpp
 * manage one probe of type DS18S20/DS18B20/DS1822
 * for the true temperature acquisition don't use directly, instead use the DSProbes class
 */

/* DS18S20 Temperature chip i/o */
/* Rouge -> bleu -> 5V
 *  jaune        -> pin9
 *  noir         -> GND
 * **************************** */

#include "Params.h"
#if USE_DS18B20


#include "DSProbe.h"

char cache[28]; // for DsProbe::getAddr()


char * DsProbe::getAddr()
{
  
    //Serial.print("R=");
    cache[0] = 0;
    String temp;
    temp = "R=";
    Serial.print(temp);
    Serial.print("(len1: "); Serial.print(temp.length()); Serial.println(")"); 
  
    //printf("%02x",(unsigned int) recv[i]);
  
      byte i;
      for( i = 0; i < 8; i++) {
          //Serial.print(probeAddr[i], HEX);
          //sprintf(cache, "R=" probeAddr[i]);
          
          String tempAddr = String(probeAddr[i], HEX);
          if(tempAddr.length() < 2)
            temp.concat("0");
          temp.concat(tempAddr);
          
          //Serial.print(" ");
          //concat(cache, " ");
          temp.concat(" ");
    }
    // e.g.:  
    //    R=28 FF 49 07 B3 17 01 50
    //    R=28 FF A7 E2 C0 17 05 8A
    Serial.print("(len2: "); Serial.print(temp.length()); Serial.println(")"); 
  
    if(temp.length()>0){
        temp.toCharArray(cache, 28);
        return cache;
    }
    return "(?)";
}
bool DsProbe::checkCrc()
{
  if ( OneWire::crc8( probeAddr, 7) != probeAddr[7]) {
    Serial.print("CRC is not valid!\n");
    return false;
  }
  return true; // OK
}
const char *DsProbe::getFamilly()
{
  switch( probeAddr[0] )
  {
    case 0x10: 
      return "DS18S20";
    case 0x28: 
      return "DS18B20";
    case 0x22: 
      return "DS1822";
    
    default:
      return "not DS18S20/DS18B20/DS1822";
  }
}


/* DS18S20 Temperature chip i/o */
/* Rouge -> bleu -> 5V
 *  jaune        -> pin9
 *  noir         -> GND
 *  Voir plutot la version _ps.ino ?
 */

#endif // #if USE_DS18B20
