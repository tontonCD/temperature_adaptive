/* file: DSProbe.h
 * manage one individual probe of type DS18S20/DS18B20/DS1822
 * for the true temperature acquisition don't use directly, instead use the DSProbes class
 */

#include "Params.h"
#if USE_DS18B20
 
#include "OneWire.h"
//OneWire  ds(9);  // on pin 9 //OBSO, using now: wireDS = OneWire(pinForWire);


typedef class
{
  byte probeAddr[8];

  char *getAddr();
  bool  checkCrc();
  const char *getFamilly();

  friend class DsProbes;
} DsProbe;




/* DS18S20 Temperature chip i/o */
/* Rouge -> bleu -> 5V
 *  jaune        -> pin9
 *  noir         -> GND
 *  Voir plutot la version _ps.ino ?
 */


typedef byte addr8[8];


#endif // #if USE_DS18B20
