/* file: DSProbes.cpp
 * manage a list of probes of type DS18S20/DS18B20/DS1822, using the OneWire class,
 * i.e. all the probes atached to a pin,
 * for individual probes see the DSProbe class
 */

#include "Params.h"
#if USE_DS18B20

#include "DSProbes.h"

#define MAX_DS18B20_SENSORS 2 // can define more, also it will use more memory
byte addr[MAX_DS18B20_SENSORS][8];

/* commands bytes */
/*
const byte Convert_T =       0x44; // Initiates temperature conversion.
const byte ReadScratchpad =  0xBE;
const byte WriteScratchpad = 0x4E;
const byte CopyScratchpad =  0x48;
const byte RecallE2 =        0xB8; // DS18B20 transmits recall status to master.
const byte ReadPowerSupply = 0xB4;
*/
typedef enum : byte
{
    Convert_T =       0x44, // Initiates temperature conversion.
    ReadScratchpad =  0xBE,
    WriteScratchpad = 0x4E,
    CopyScratchpad =  0x48,
    RecallE2 =        0xB8, // DS18B20 transmits recall status to master.
    ReadPowerSupply = 0xB4
} CommandBytes;


int DsProbes::scanDevices(int pinForWire)
{
    numProbes = 0;
    if(pinForWire >= 0)
        wireDS = new OneWire(pinForWire);
    else
        wireDS = new OneWire();
    // NOTE: pin should be SS = D10; wrong if MMCP: use D9
    // if change, @see "Slave Select" at http://www.gammon.com.au/spi (uncheck)

    // Search for the next device
    // donc on obtient toujours (1)...\n, (2)...\n, (3)...\n,  etc, "No more addresses.\n"
    byte tempAddr[8];
  
    // got to end, we'll start again later
    while( wireDS->search(tempAddr) )
        ;
/* ?
    reset_search();
    delay(250);
 */
 /* Loop with search(addrArray):
    *  Search for the next device. 
    *  The addrArray is an 8 byte array. 
    *  If a device is found, addrArray is filled with the device's address and true is returned. If no more devices are found, false is returned. 
    */
  for(;;)
  {
    //byte *addrPtr[8];
    //addrPtr = probes[numProbes].probeAddr;
    
    if( wireDS->search(probes[numProbes].probeAddr) )   // defined as: byte probeAddr[8];
    {
      char *temp = probes[numProbes].getAddr(); // unused temp

      // check CRC  // TODO displace
      bool check = probes[numProbes].checkCrc();
      if(check)
        numProbes++; // OK
    }
    else
      // done
      return numProbes; // num probes found
  }
}

float DsProbes::getProbeTemp(int numProbe)
{
  if(numProbe >= numProbes)
  {
    Serial.println(" getProbeTemp: ERROR with numProbe");
    return 0.0; // ERROR
  }
  
  byte i;
  byte present = 0;
  byte data[12];
  addr8 &addr = probes[numProbe].probeAddr;

  // "Convert T" (Convert_T) byte command
  wireDS->reset(); // Usually this is needed before communicating with any device. 
  wireDS->select(addr);
  //wireDS->write(Convert_T,1);     // start conversion, with parasite power on at the end
  wireDS->write(Convert_T);         // start conversion, with Normal

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  // Read Scratchpad
  present = wireDS->reset();
#pragma unused(present)
  wireDS->select(probes[numProbe].probeAddr);    
  wireDS->write(ReadScratchpad);         


  for ( i = 0; i < 9; i++) {           // we need 9 bytes
      data[i] = wireDS->read();
  }

  int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;

  LowByte = data[0];
  HighByte = data[1];
  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }

     /* convert bytes to temperature 
      *  - for 12bits resolution, 0b1000 gives 0.5°, so, as the docs say, multiply by (0.0625)
      *  optimisation :  divide with 16.
      */

#define use_ints          false
#define use_ints_separate false
#define use_floats        true
#if use_ints
     /*  - we want 2 decimals => use ints and multiply by (100 * 0.0625) or 6.25
      *  function as doc:           Tc_100 = TReading * 6.25
      *  optimisation (no float):   Tc_100 = (6 * TReading) + TReading / 4;
      *  optimisation (better):     Tc_100 = TReading / 16;
      *  optimisation (faster):     Tc_100 = TReading >> 4;
      */
      int Tc_100 = (6 * TReading) + TReading / 4;  // previous code (obso)
#endif
   
#if use_floats
    float Tc_100_f = float(TReading)/16.0;
#endif

#if use_ints_separate
    /* separate as "Whole.Fract" where both are int */
  int Whole, Fract;
  Whole = Tc_100 / 100;  // separate off the whole and fractional portions
  Fract = Tc_100 % 100;
#pragma unused(Whole, Fract)
  //sprintf(buf, "%d:%c%d.%d\337C     ",sensor,SignBit ? '-' : '+', Whole, Fract < 10 ? 0 : Fract);
#endif

// for DEBUG
#if false
  Serial.print("Temp on ");Serial.print(numProbe);
  Serial.print(" (");Serial.print(probes[numProbe].getAddr());Serial.print("): ");
  Serial.print(Tc_100);
  Serial.print(" ->   ");Serial.print(Whole);Serial.print(".");
  if(Fract<10)
    Serial.print(0);
  Serial.println(Fract);
#endif


#if use_ints
  float result = float(Tc_100)/100.0;
  
  // compare int and float versions:
  Serial.print("*****Temp on ");Serial.print(numProbe);Serial.print(": "); Serial.print(result);
  Serial.print("\t/ ");Serial.println(Tc_100_f);
  // => ok, and two decimals too!
  
  return result;
#endif

#if use_floats
    return Tc_100_f;
#endif
}

const char *DsProbes::getProbeAddr(int numProbe)
{
  if(numProbe >= numProbes)
  {
    Serial.println(" getProbeAddr: ERROR with numProbe"); 
    return "(error)";
  }
  Serial.print(">"); // debug
  return probes[numProbe].getAddr();
}


#endif // #if USE_DS18B20
