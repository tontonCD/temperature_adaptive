/* file: DSProbes.h
 * manage a list of probes of type DS18S20/DS18B20/DS1822, using the OneWire class,
 * i.e. all the probes atached to a pin,
 * for individual probes see the DSProbe class
 */

#include "Params.h"
#if USE_DS18B20


#include "DSProbe.h"

/** DsProbes (with an ending 's') managed an array of DsProbe instances */
typedef class DsProbes
{
  DsProbe probes[5];    // limit 5 (can change it)
  int numProbes = 0;
  OneWire *wireDS;		// the wire, one wire with one or more probes

public:
  int scanDevices(int pinForWire = -1);
  float getProbeTemp(int numProbe);
  const char *getProbeAddr(int numProbe);
} DsProbes;


typedef byte addr8[8];


#endif // #if USE_DS18B20
