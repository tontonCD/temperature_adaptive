/* HeatController.cpp */

/* WARNING:
 *  Proceeding with an incorrect placement of the probe, may result in a constant alimentation of the Heater! 
 *  Be aware of what can occure if the Heater is in a constant "On" state: fire, ...
 *  KEEP-ON SURVEY Your System!
 */

 /* PB
  *  the scan for maxTemp can occur too early, (while temp is decreasing) so you (must) check for
  *  a minimal delay before scan, this (should be) done by predictive time to reach maxTemp 
  */

#include "Params.h"
  
#include "HeatController.h"

const char *GlobalStepChars[] =   { 
  "gsPending_Idle",
  "gsHeating_First",
  "gsPending",
  "gsHeatingOn", 
  "gsHeatingIdle",
  "gsOffScanning",
  "gsOff"
};

#include <Arduino.h>




/* ** PARAMS ** */
float setting_temp =     40.00;
float gTemperatureCorrection = 0.07;
/* NOTE : the Probe precision is not 0.01!, e.g. mesures are always:
 * 564024: Temp: 40.25
 * 568096: Temp: 40.18
 * 571151: Temp: 40.12
 * 575223: Temp: 40.06
 * 578279: Temp: 40.00
 */
unsigned long gStepDuration;

unsigned long gMaxDuration = 5000; // ms, for heater
unsigned long gMinDuration =  500; // ms, for heater


unsigned long HeatController::hDefaultDuration = 1000;
int           HeatController::hPinForHeater =      10;

HCGlobalState _logChangeStepTo(HCGlobalState iNewStep);

/* *** constructor *** */
HeatController::HeatController()
{
  pinMode(hPinForHeater, OUTPUT);
  digitalWrite(hPinForHeater, HIGH);
  

  currentStepPtr = new HeatControllerStep();

  hTotal_Start_IdleTime = 10000; // 10s
  hCurrentStep_Logged = false;

  unsigned long heatingTime = 1000; 
  unsigned long totalTime = 1000; // i.e. stepDuration
  
  currentStepPtr->setRatio(totalTime, heatingTime);
  hCurrentState = _logChangeStepTo(gsPending_Idle);
};

HeatController::~HeatController()
{
  delete currentStepPtr;
}

/* ** setters/getters ** */
void          HeatController::setTempTarget(float iTargetTemp, int iPinForHeater)
{
  hTargetTempValue = iTargetTemp;
  hPinForHeater = iPinForHeater;

  //init this at all setTempTarget
  hTempMargin = hInitialTempMargin;       ;
  hTargetTempValueWithMargin = hTargetTempValue - hTempMargin;
}



/* needs setReady() */
HCGlobalState HeatController::checkStepV3(float iCurrentTemp, unsigned long iCurrentTime)
{
    Serial.println("----------");
    Serial.print(iCurrentTime);  Serial.print(":\t");
    Serial.print("  HC::checkStepV3; state: "); Serial.println(GlobalStepChars[hCurrentState]);
    Serial.print("  "); Serial.print(iCurrentTemp); Serial.print("°\n");
  ///static HCSState currentStepState; // ???: check if planed to be used
  HCSState        newStepState;
  ///unsigned long lastTimeOn;

  static float          lastTemperature = 0;//???
  static unsigned long  lastTime = 0;//???

  
  float newRatio;
  unsigned long newHeatingTime;
  ///unsigned long newTotalTime; //???


    // declare here all temporary variables, for optimisation (the gsHeatingOn case takes 1022ms!)
    float         lastDiffTemp;
    unsigned long lastDiffTime;
    unsigned long newPercent;

    switch(hCurrentState)
    {
        case gsPending_Idle:
            break;
            
        case gsHeating_First:
            /* same as , but ratio is undefined; just precise the heatingTime */
            
            if(iCurrentTemp < lastTemperature)
            {
                newHeatingTime = kFirstHeatDuration; // FIX, was: newHeatingTime = kStepDuration;
            } else
                newHeatingTime = 0;

            if(newHeatingTime < kStepDuration)  // normal way
                currentStepPtr->setRatio(kStepDuration, newHeatingTime);
            else                                // only for the first time, kFirstHeatDuration can be higher than the nominal kStepDuration
                currentStepPtr->setRatio(newHeatingTime, newHeatingTime);
            
            newStepState = currentStepPtr->action(hPinForHeater);
            
            hCurrentState = _logChangeStepTo(gsHeatingOn);
            break;
            
            
        case gsPending:
            break;

        case gsHeatingOn:
            // check current temperature, so we may adjust the heater ratio
            
            ///lastTimeOn = HeatControllerStep::getLastTimeOn(); // ???: used?
            
            ///newRatio = computeRatio(iCurrentTemp);
            
            /* newRatio = ( temp(t) - temp(t-1) ) / time  */
            
            
            lastDiffTemp = iCurrentTemp - lastTemperature;
            //Serial.print("  lastDiffTemp:"); Serial.println(lastDiffTemp); // OK
            
            //unsigned long lastDiffTime = currentStepPtr->effectiveHeatingTime(); the value is false
            lastDiffTime = iCurrentTime - lastTime;
            //Serial.print("  lastDiffTime:"); Serial.println(lastDiffTime); // can be higher than kStepDuration because of float calculations time
            
            newRatio = lastDiffTemp / (float(lastDiffTime));
      
            if(newRatio > hRatio)
            {
                hRatio = newRatio;
            }
            Serial.print("  current ratio:     ");  Serial.print(newRatio*1000); Serial.println(" °/s");
            Serial.print("  current max ratio: ");  Serial.print(hRatio  *1000); Serial.println(" °/s");
            
            if(iCurrentTemp < hTargetTempValueWithMargin)
                newHeatingTime = ( float(hTargetTempValueWithMargin)-float(iCurrentTemp) )  / hRatio;
            else                                            // => (iCurrentTemp > hTargetTemp) => do Off
            {
                newHeatingTime = 0;
                
                // avoid full hTempMargin from now; not the most really good place.
                // should wait (temp>hTargetTempValueWithMargin) followed by (temp<hTargetTempValueWithMargin)
                if(hTempMargin > 0)
                    hTempMargin = hTempMargin - 1.0;
                hTargetTempValueWithMargin = hTargetTempValue - hTempMargin;
            }
      
            ///newTotalTime = hCurrentStep_StopTime - hCurrentStep_StartTime;

            /*
            Serial.print("  temp HeatingTime: ");  Serial.println(newHeatingTime);
            if(newHeatingTime > kStepDuration)
                newHeatingTime = kStepDuration;
            */
            
            //Serial.print("   params / hTargetTempValueWithMargin: ");Serial.println(hTargetTempValueWithMargin);
            //Serial.print("   params / iCurrentTemp");Serial.println(iCurrentTemp);
            
            Serial.print("  newHeatingTime:   ");  Serial.print(newHeatingTime);
            newPercent = newHeatingTime *100 / kStepDuration;
            Serial.print(" (power: ");  Serial.print(newPercent); Serial.println("%)"); //Serial.println(" °/s");
            
            currentStepPtr->setRatio(kStepDuration, newHeatingTime);
            
            newStepState = currentStepPtr->action(hPinForHeater);

#if 0
            if(newStepState != currentStepState)
            {
                if(     currentStepState==hcssOn) // means: On -> Idle
                {
                    ///hRatio = iCurrentTemp
                }
                else if(newStepState    ==hcssOn) // means: Idle -> On
                {
                    ///hRatio = iCurrentTemp
                }
                else
                    Serial.println("    ERROR in HeatController::checkStepV3()");
            }
#endif
      


        //}
            break;

        case gsHeatingIdle:
            break;
        case gsOffScanning:
        case gsOff:
            break;
    }

    lastTemperature = iCurrentTemp;
    lastTime = iCurrentTime;

    return hCurrentState;
}

HCGlobalState HeatController::setReady()
{
    hCurrentState = _logChangeStepTo(gsHeating_First);
    return hCurrentState;
}


/* shema (V2):
 *  gsPending_Idle  ->  gsHeating_First ->  gsHeatingIdle   -> gsOffScanning    -> gsPending
 *                                              ^                                       V
 *                                              |---------------------------------------|
 * - While gsHeatingIdle, heating is active
 *  it checks the Temperature that "will" be reached if maintained
 */
 
// TODO: unused, keep for refactoring the current ratio computation
// float HeatController::computeRatio(float iCurrentTemp)





/*
void HeatController::updateAllStep()
{
  Serial.print("** updateAllStep (params): ");  Serial.println();
  unsigned long lastDiffTime = hMaxTempTime - hCurrentStep_StartTime;
  float         lastDiffTemp = hMaxTempValue - hCurrentStep_StartTemp;
  hCurrentStep_HeatingTime = currentStepPtr->effectiveHeatingTime();
  Serial.print("   hCurrentStep_StartTime: "); Serial.println(hCurrentStep_StartTime);
  Serial.print("   hCurrentStep_StopTime : "); Serial.println(hCurrentStep_StopTime);
  Serial.print("   effectiveHeatingTime  : "); Serial.println(hCurrentStep_HeatingTime);
//Serial.print("** computeRatio: "); Serial.print(lastDiffTemp);
  hAllSteps_DiffTime += lastDiffTime; // UNUSED
  hAllSteps_HeatingTime += hCurrentStep_HeatingTime;
  hAllSteps_DiffTemp += lastDiffTemp;
  Serial.print("   => hAllSteps_DiffTime   : "); Serial.println(hAllSteps_DiffTime);
  Serial.print("   => hAllSteps_HeatingTime: "); Serial.println(hCurrentStep_HeatingTime);
  Serial.print("   => hAllSteps_DiffTemp   : "); Serial.println(hAllSteps_DiffTemp);
}
*/


float HeatController::computeRatio(float iCurrentTemp)
{
  //unsigned long remainingTime = currentStepPtr->doOn();
  //return remainingTime;

/*
  unsigned long hcsExpectedTime;

  unsigned long hcsDoneTime;
  unsigned long hcsRatio;
*/
  //unsigned long hcExpectedTime;
  //unsigned long hcDoneTime;
  //unsigned long hcRatio;
   

  ///unsigned long lastDiffTime = hMaxTempTime - hCurrentStep_StartTime;UNUSED
  ///float         lastDiffTemp = hMaxTempValue - hCurrentStep_StartTemp;
  ///hCurrentStep_HeatingTime = currentStepPtr->effectiveHeatingTime();
#if true
  Serial.print("** computeRatio (params): ");  Serial.println();
  ///Serial.print("   hCurrentStep_StartTime: "); Serial.println(hCurrentStep_StartTime);
  ///Serial.print("   hCurrentStep_StopTime : "); Serial.println(hCurrentStep_StopTime);
  ///Serial.print("   effectiveHeatingTime  : "); Serial.println(hCurrentStep_HeatingTime);
  Serial.print("    hMaxTempTime          : "); Serial.println(hMaxTempTime);
  Serial.print("    hMax80TempTime        : "); Serial.println(hMax80TempTime);
  Serial.print("    now                   : "); Serial.println(millis());
#endif

  float newRatio;
  
//#if false // use last step
  Serial.print("** computeRatio (1): "); 
  float         lastDiffTemp = hMaxTempValue - hCurrentStep_StartTemp;
  Serial.print(lastDiffTemp);
  //Serial.print("° in "); Serial.print(lastDiffTime); Serial.print("ms =>"); WRONG
  Serial.print("° for "); Serial.print(hCurrentStep_HeatingTime); Serial.print("ms =>");
  
  newRatio = lastDiffTemp / (float(hCurrentStep_HeatingTime));
  
  //Serial.print("  lastDiffTime: "); Serial.print(lastDiffTime);
  //Serial.print("; lastDiffTemp: "); Serial.print(lastDiffTemp);
  Serial.print("   ratio: ");  Serial.print(newRatio*1000); Serial.println(" °/s");
//#else     // use all steps
  Serial.print("** computeRatio (2): "); 
  Serial.print(hAllSteps_DiffTemp);
  //Serial.print("° in "); Serial.print(lastDiffTime); Serial.print("ms =>"); WRONG
  Serial.print("° for "); Serial.print(hAllSteps_HeatingTime); Serial.print("ms =>");
  newRatio = hAllSteps_DiffTemp / (float(hAllSteps_HeatingTime));
  //Serial.print("  lastDiffTime: "); Serial.print(lastDiffTime);
  //Serial.print("; hAllSteps_DiffTemp: "); Serial.print(lastDiffTemp);
//#endif
 
  Serial.print("   ratio: ");  Serial.print(newRatio*1000); Serial.println(" °/s");

/*
  unsigned long newHeatingTime = float(hTargetTempValueWithMargin-iCurrentTemp) / hRatio;
  unsigned long newTotalTime = lastDiffTemp;
  currentStepPtr->configure(newHeatingTime, newTotalTime);
*/
  return newRatio;

  // unsigned long remainingTime = float(hTargetTempValueWithMargin-iCurrentTemp) / ratio;
}




HCGlobalState _logChangeStepTo(HCGlobalState iNewStep)
{
    unsigned long nowTime = millis();
    Serial.print(nowTime);  Serial.print(":\t");
    Serial.print("  HC: ** State changing to: (");Serial.print(iNewStep);Serial.print(") = ");
    Serial.println(GlobalStepChars[iNewStep]);
    return iNewStep;
}
      
