/* file: HeatController.h */

#ifndef HeatController_H
#define HeatController_H

#include "HeatControllerStep.h"


typedef enum  {
  gsPending_Idle = 0, // idle at launch
  gsHeating_First,    // idle after gsPending_Idle, first call to gsHeatingOn (ration is not yet defined)
  gsPending,          // idle after gsOffScanning
  gsHeatingOn,      // OBSO
  gsHeatingIdle,    // allows looping between On and Idle, e.g. 1mn at 50% => 3 X (10S + 10s)
  gsOffScanning,    // no heating (done), just waiting for max Temperature // ???: displace
  gsOff // USELESS ?
} HCGlobalState;    // was: HCGlobalStep



class HeatController
{
  ///static float hDefaultRatio = 1000;
  static unsigned long hDefaultDuration; // first duration; todo: param
  static int hPinForHeater;

    HCGlobalState   hCurrentState = gsPending_Idle; // was: hCurrentStep

  HeatControllerStep *currentStepPtr = 0; //&stepN

    // V3
    /* Temp = TempAtStart + delay + coef * heatTimeFromT0 */
    float           hTempAtStart;
    unsigned long   hTimeAtStart;
    unsigned long   hDelay;
    float           hCoef;
    //unsigned long   hHeatTimeFromT0;
    float           hTempAtT2;
    unsigned long   hTimeAtT2;

    
    
    
  unsigned long hTotal_StartTime;// = currentTime;
  float         hTotal_StartTemp;
  
  float         hTotal_StartMinTemp;// = currentTemp;
  float         hTotal_StartMaxTemp;// = currentTemp;
  unsigned long hTotal_Start_IdleTime;

  /* current step */
  float         hCurrentStep_StartTemp;// = currentTemp;
  unsigned long hCurrentStep_StartTime;// = currentTime;
  unsigned long hCurrentStep_StopTime;
  unsigned long hCurrentStep_HeatingTime;
  bool          hCurrentStep_Logged;
  /* all step */
  unsigned long hAllSteps_DiffTime;
  unsigned long hAllSteps_HeatingTime;
  float         hAllSteps_DiffTemp;

  
  unsigned long nextTime = 0;
  float hRatio = 0.1/1000; // will be compute in the first sequence; (would update in the next sequences); fix it to a non-zero value


  unsigned long hcExpectedTime;
  //unsigned long hcRatio;
  unsigned long hcDoneTime;
  unsigned long hcDoneTimeRemainingTime;



  /* configuration */
  float         hTargetTempValue = 0;
  float         hTargetTempValueWithMargin = 0;
  float         hInitialTempMargin = 8.0; // PARAM, integer
  float         hTempMargin;

  /* track for hMax80TempValue, while temp is downing */
  float         hMaxTempValue   = 0;        /**< max temperature since OnState began  */
  unsigned long hMaxTempTime;               /**< time at witch we found it            */
  float         hMax80TempValue   = 0;      /**< temp value for 80% between startTemp end maxTemp */
  unsigned long hMax80TempTime;                 // time at witch we found it
  
 
  float computeRatio(float currentTemp);  // setStepTo(float iTemp, unsigned long iTime);
  void setStepToOffGrowing(float iTemp, unsigned long iTime);
  void setStepToOffDawning(float iTemp, unsigned long iTime);

public:
    /**
     @abstract delays as the closed next millis() function returns a value rounded to roundMs;
     @note: old: the effective delay is always at least 500ms;
     @since V3.1: 500ms min delay removed, you are responsible to add it in your main code */
    ///static void smartDelay(unsigned long roundMs); // renamed from delay_Optimized()
    /** same as smartDelay(unsigned long roundMs); the effective delay is always at least 500ms;*/
    ///static void smartDelayWith500(unsigned long roundMs); // renamed from delay_Optimized()

    HeatController();
    ~HeatController();
    
    /**
     @param targetTemp the expected Temperature
     @param pinForHeater the pin to be active for action */
    void        setTempTarget(float targetTemp, int pinForHeater);

    
  /**< computes a new current state and returns it */
  HCGlobalState checkStepV3(float currentTemp, unsigned long currentTime); 
  HCGlobalState setReady();

private:
    ///void updateAllStep();
    
    ///unsigned long doStartV3(int heaterPin, unsigned long iTime);
    ///unsigned long doOnV3(int heaterPin, unsigned long iTime);

};


#endif
