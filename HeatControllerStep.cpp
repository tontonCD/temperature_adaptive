/* file: HeatControllerStep.cpp */

#include "HeatControllerStep.h"
#include <Arduino.h> // just for Serial.println()

void HeatControllerStep::smartDelay(unsigned long iRoundMs)
{
  unsigned long currentTime = millis();
  unsigned long optDelay = (iRoundMs - (currentTime % iRoundMs)); 

    Serial.print(currentTime);  Serial.print(":\t");
    Serial.print("   HCS::smartDelay, for: "); Serial.println(optDelay);
    delay(optDelay);
}



/* new shema (V3):
    if iHeatingTime == iTotalTime
    => set to On and return; if currently On don't change

    if iHeatingTime < iTotalTime
    * set to On; set time to off
    * next time in the function, check effective time and ajust
 */
unsigned long hcsStepDuration;
unsigned long hcsHeatDuration;
unsigned long hcsIdleDuration; // for optimisation, always: hcsStepDuration - hcsHeatDuration
unsigned long hcsLastHeatDuration; // different from hcsLastTimeOn
unsigned long hcsLastIdleDuration; // different from hcsLastTimeOff


void HeatControllerStep::setRatio(unsigned long iTotalTime, unsigned long iHeatingTime)
{
    hcsStepDuration = iTotalTime;       // was: iStepDuration
    hcsHeatDuration = iHeatingTime;     // was: iHeatDuration


    if(hcsHeatDuration > hcsStepDuration)
    {
        Serial.println("   HCS:: WARNING : heatDuration > stepDuration");
        ///hcsHeatDuration = hcsStepDuration;
        hcsIdleDuration = 0;
    } else
        hcsIdleDuration = hcsStepDuration - hcsHeatDuration;
}

unsigned long hcsLastTimeOn;
unsigned long hcsLastTimeOff;
unsigned long HeatControllerStep::getLastTimeOn()    {
    return hcsLastTimeOn;
}

HCSState HeatControllerStep::action(int heaterPin) // ???: could return hcsLastTimeOn
{
    unsigned long nowTime = millis();
    unsigned long remainingTime;
    Serial.print(nowTime); Serial.print(":\t");
    Serial.println("   HCS::action");

    // assume we enter her with Off State, unless the ration is 100%

    // Full cycle, two cases
    if(       hcsHeatDuration >= hcsStepDuration) // 100% ratio
    {
        /* do On */
        if(hcsState == hcssIdle)
        {
            Serial.print("   HCS:: Switching to On for "); Serial.print(hcsHeatDuration); Serial.println("ms");
            nowTime = millis();
            digitalWrite(heaterPin, LOW);
            hcsLastTimeOn = nowTime;
            hcsLastIdleDuration = nowTime - hcsLastTimeOff;
            hcsState = hcssOn;
        }
        else
        {
            nowTime = millis();
            hcsLastHeatDuration = nowTime - hcsLastTimeOn;
            // remaining time: (hcsHeatDuration - hcsLastHeatDuration); can be negative ?
            if(hcsLastHeatDuration >= hcsHeatDuration)
            {
                Serial.println("   HCS:: Switching to Off");
                digitalWrite(heaterPin, HIGH);
                hcsState = hcssIdle;
            } else
            {
                remainingTime = hcsHeatDuration - hcsLastHeatDuration;
                Serial.print("   HCS:: still On for "); Serial.print(remainingTime); Serial.println("ms");
                if(remainingTime < 1000) // supposed reaction time until the next loop
                {
                    delay(1000 - remainingTime);
                    Serial.println("   HCS:: Switching to Off");
                    digitalWrite(heaterPin, HIGH);
                    hcsState = hcssIdle;
                }
            }
        }
    } else if(hcsHeatDuration ==0 )             // 0% ratio
    {
        /* do Off */
        if(hcsState == hcssOn)
        {
            nowTime = millis();
            digitalWrite(heaterPin, HIGH);
            hcsLastTimeOff = nowTime;
            hcsLastHeatDuration = nowTime - hcsLastTimeOn;
            hcsState = hcssIdle;
        }
    }
    
#if 1 // fix: try to return before the whole Step ends -> OK
    else if(hcsState == hcssOn)
    {
        nowTime = millis();
        if(nowTime - hcsLastTimeOn >= hcsHeatDuration)
        {
            /* do Off */
            // ???: check if difference is significative, so we should reduce the next On (or augment the next Off ?)
            // remember that they are UNSIGNED!
            Serial.println("   HCS:: Switching to Off");
            digitalWrite(heaterPin, HIGH);
            hcsLastTimeOff = nowTime;
            hcsLastHeatDuration = nowTime - hcsLastTimeOn;
            hcsState = hcssIdle;
        }
    } else if(hcsState == hcssIdle)
    {
        nowTime = millis();
        if(nowTime - hcsLastTimeOff >= hcsIdleDuration)
        {
            /* do On */
            Serial.print("   HCS:: Switching to On for "); Serial.print(hcsHeatDuration); Serial.println("ms");
            
            digitalWrite(heaterPin, LOW);
            hcsLastTimeOn = nowTime;
            hcsLastIdleDuration = nowTime - hcsLastTimeOff;
            hcsState = hcssOn;
            
            if(hcsHeatDuration < 1000) // supposed reaction time until the next loop
            {
                delay(1000 - hcsHeatDuration);
                Serial.println("   HCS:: Switching to Off");
                digitalWrite(heaterPin, HIGH);
                hcsState = hcssIdle;
                // TODO: factorize
                // TODO: lastTimeOff
                // TODO: for low values of HeatDuration, the real time is too long
            }
        }
    } else
    {
        Serial.println("    ERROR in HeatControllerStep::action()");
    }
#else
    else
    {
        nowTime = millis();
        /* On -> delay -> off */
        digitalWrite(heaterPin, LOW);
        delay(hcsHeatDuration);
        digitalWrite(heaterPin, HIGH);
        hcsLastHeatDuration = hcsHeatDuration;
        hcsLastIdleDuration = hcsIdleDuration;
        // the delay for hcsIdleDuration will be performed in smartDelay()
    }
#endif

    delay(10);
    HeatControllerStep::smartDelay(hcsStepDuration / 2);
    return hcsState;
}

/*
HCSState HeatControllerStep::doStart(int heaterPin)
{
    
}
 */

#if 0 // OBSO ?
HCSState HeatControllerStep::doOn(int heaterPin)
{
    unsigned long currentTime = millis();
    unsigned long duration;
    
    switch(hcsState)
    {
        case hcssPending:
            hcsStartTime = currentTime;
            
            duration = _doOn(heaterPin);
            hcsNextTimeToOn = currentTime + 1000 - duration;
            Serial.println("    state: hcssPending");
            Serial.print(  "      heating for "); Serial.print(duration); Serial.println("ms");
            Serial.print(  "      starting at "); Serial.println(currentTime);
            Serial.print(  "      next at     "); Serial.println(hcsNextTimeToOn);
            
            if(hcsDoneHeatingTime >= hcsExpectedHeatingTime)
            {
                Serial.println("    state becomes: hcssDone");
                hcsState = hcssDone;
            } else
            {
                hcsState = hcssIdle;
            }
            break;
            
        case hcssOn:
            Serial.println("ERROR: state is hcssOn");
            hcsState = hcssIdle;
            break;
            
        case hcssIdle:
            if(currentTime >= hcsNextTimeToOn)
            {
                duration = _doOn(heaterPin);
                hcsNextTimeToOn = currentTime + 1000;
                hcsState = hcssIdle; // no change
                Serial.println("    state: hcssIdle");
                Serial.print(  "      heating for "); Serial.print(duration); Serial.println("ms");
                Serial.print(  "      starting at "); Serial.println(currentTime);
                Serial.print(  "      next at     "); Serial.println(hcsNextTimeToOn);
            }
            
            if(hcsDoneHeatingTime >= hcsExpectedHeatingTime)
            {
                Serial.print("      state becomes: hcssDone");Serial.println();
                hcsState = hcssDone;
            } 
            //else
            //{
            //  hcsState = hcssIdle;
            //}
            
            break;
            
        case hcssDone:
            Serial.println("ERROR: state is hcssDone");
            break;
            
    }
    return hcsState;
}
#endif



unsigned long HeatControllerStep::effectiveHeatingTime() 
{ return hcsStepHeatingTime; }
