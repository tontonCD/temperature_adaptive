
/* file: HeatControllerStep.h */

typedef enum HCSState {
  hcssPending = 0,    // idle // !!!: OBSO ?
  hcssOn,
  hcssIdle,  
  hcssDone
} HCSState;


/* we loop on { heat some time + delay some time } until hcsExpectedTime is reached */
class HeatControllerStep
{
	unsigned long hcsExpectedHeatingTime;
///	unsigned long hcsExpectedTotalTime;
    // NEW (?)
///    unsigned long hcsT0; // start
///    unsigned long hcsT1; //
///    unsigned long hcsTAct;
    
	unsigned long hcsDoneHeatingTime;
	//unsigned long hcsRatio;

	unsigned long hcsStepHeatingTime;
///	unsigned long hcsStepDelayTime;

	HCSState hcsState = hcssIdle; // !!!: not the good place
	unsigned long hcsStartTime;
	unsigned long hcsNextTimeToOn;

  /* return the effective heating time, can be zero */
	///unsigned long _doOn(int heaterPin);

public:
	static void smartDelay(unsigned long roundMs);
	static unsigned long getLastTimeOn();
	
	///void init(unsigned long heatingTime, unsigned long totalTime);
	
	/** changes the Ratio; 
	@param stepDuration a whole-step duration for the heater, usually one On then one Off in this time
	@param heatDuration the time for On as part of a stepDuration; if value are equal, it correspond to a ration of 100%
	@note if equals, the action() function returns while On.
	@since V3.1
	*/
	void setRatio(unsigned long stepDuration, unsigned long heatDuration);// !!!: replaces init()

	/** returns true when done; OBSO */
	///HCSState doOn(int heaterPin);

	/** main action. Heats for heatDuration, Idles for (stepDuration-heatDuration) but really waits until the ens of multiples of heatDuration
		@since V3.1
	*/
	HCSState action(int heaterPin); // ???: name is correct, interfer with anything else ?


  unsigned long  effectiveHeatingTime();
};
    
