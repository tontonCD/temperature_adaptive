/* HeatControllerStep.cpp */


#include "_HeatControllerStep.h"

//#include <serial.h>
#include <Arduino.h>

static unsigned long HeatControllerStep::minTime;// = 0;
static unsigned long HeatControllerStep::maxTime;// = 0;

/*
char *getStepName(Step iState)
{
  switch(iState)
  {
    case sOn:
      return ("STATE - ON");
    case sOffGrowing:
      return ("STATE - OFF - GROWING");
    case sOffDawning:
      return ("STATE - OFF - DAWNING");
    case sNone:
    default:
      return ("sNone");
  }
}*/
char *getStepName[] = {
  "(none)", "STEP - ON", "STEP - OFF - GROWING", "STEP - OFF - DAWNING"
};
char *HeatControllerStep::getCurrentStepName() {
  return getStepName[identifier];
}

/*
char *getActionName(ControlllerAction iAction);
{
  switch(iAction)
  {
    case cIgnore:   // before, i.e. off
      return ("cIgnore");
    case oBetween:  // between; i.e. Active
      return ("cDoOn");
    case oAfter:    // after, i.e. off again
      return ("cDoOff");
    default:
      return ("sNone");
  }
    cIgnore =0, // before
  cDoOn,  // becomes Active
  cDoOff  // no more Active

}
*/
char *getActionName[] = {
  "cIgnore", "cDoOn", "cDoOff"
};


void HeatControllerStep::startStep(float iTemp, unsigned long iTime)
{
  startTime = iTime;
  startTemp = iTemp;
  maxTemp = startTemp;

  minTime = iTime + timeLengthMin;
  if(timeLengthMinMax>0)
    maxTime = iTime + timeLengthMinMax;
  else
    maxTime = 0; // i.e. ignore
  Serial.print("Starting Step -> ");Serial.println(getStepName[identifier]);
  Serial.print("  current timeLengthMinMax is: ");Serial.println(timeLengthMinMax);
  Serial.print("  =>with minTime / maxTime :");  
  Serial.print(minTime); Serial.print(", "); Serial.println(maxTime); 
  onState = oBefore;
  
  /* trace */
  // String test;
  if(timeLengthMin>0)
  {
    Serial.print("   Would stay for: ");Serial.print(timeLengthMin);Serial.println(" S ");
  }
  Serial.print("   temp=");  Serial.println(iTemp);
  //Serial.print(" for "); Serial.print(theDelay);Serial.println("ms");
}

ControlllerAction HeatControllerStep::setStateIfDiffers(OnState iNewtate)
{
  if(onState==iNewtate)
  {
    Serial.print("  still "); Serial.print(onState); Serial.println(" => cIgnore");
    return cIgnore;
  }
  Serial.print("  from "); Serial.print(onState); Serial.print(" to "); Serial.print(iNewtate);
  onState = iNewtate;
  ControlllerAction result = cDoOff;
  if(iNewtate==oBetween)
    result = cDoOn;
  // else doOff
  Serial.print(" => ");Serial.println(getActionName[result]);
  return result;
}
ControlllerAction HeatControllerStep::checkStep(float iTemp, unsigned long iTime)
{

  //Serial.print("current stateOn.timeLengthMinMax: ");Serial.println(timeLengthMinMax);
  //Serial.print("iTime / minTime / maxTime :");  
  //Serial.print(iTime); Serial.print(", "); Serial.print(minTime); Serial.print(", "); Serial.println(maxTime); 
  Serial.print(" Checking Step :");Serial.println(getStepName[identifier]);
  Serial.print("  iTime / minTime / maxTime: ");  
  Serial.print(iTime); Serial.print(", "); Serial.print(minTime); Serial.print(", "); Serial.println(maxTime); 

  // before => currently off
  if(iTime < minTime)
  {
    //Serial.println(" cIgnore because iTime < minTime");
    //return cIgnore;
    return setStateIfDiffers(oBefore);
  }

  // after => currently off
  if(iTime >= maxTime && maxTime!=0)
  {
    /*
    if(onState == oAfter)
    {
      Serial.println(" cIgnore because onState is oAfter");
      return cIgnore;
    }
    Serial.println(" cDoOff because onState becomes oAfter");
    onState = oAfter;
    return cDoOff;*/
    return setStateIfDiffers(oAfter);
  }

  // between => on
  /*
  if(onState == oBetween)
  {
    Serial.println(" cIgnore because currentlyOn is oBetween");
    return cIgnore;
  }
  Serial.println(" cDoOn because onState becomes oBetween");
  onState = oBetween;
  return cDoOn;
  */
  return setStateIfDiffers(oBetween);
}
