/* HeatControllerStep.h */

typedef enum Step
{
  sNone =0,
  sOn,
  sOffGrowing,
  sOffDawning
};




typedef enum ControlllerAction
{
  cIgnore =0, // before
  cDoOn,  // becomes Active
  cDoOff  // no more Active
};
///char *getStepName(Step iStep);


typedef enum OnState // better than bool, because time can change from before to after
{
  oBefore =0, // before, i.e. off
  oBetween,   // between; i.e. Active
  oAfter      // after, i.e. off again
};
///char *getActionName(ControlllerAction iAction);


class HeatControllerStep
{
  Step identifier;
  ControlllerAction action; /// USED ?
  
  unsigned long startTime = 0;    // time where this Step began
  float startTemp = 0;            // temperature where OnState began
  float maxTemp   = 0;            // max temperature since OnState began
  OnState onState;
  
  unsigned long timeLengthMin = 0;    // time to wait before next Step, 		e.g. stateNone must stay minimum this
  unsigned long timeLengthMinMax = 0; // time max to stand before next Step from startTime, if not null 	e.g. stateOn can't stay more than this; 
  
  static unsigned long minTime;//
  static unsigned long maxTime;
  char *getCurrentStepName();
  
  ControlllerAction setStateIfDiffers(OnState iNewtate);

  friend class HeatController;

  void startStep(float iTemp, unsigned long iTime);
  ControlllerAction checkStep(float iTemp, unsigned long iTime);
};
