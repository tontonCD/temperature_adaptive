// file: Param.cpp

#include "Params.h"
//#include <Arduino.h> // just for definition of A5

/** a Step is a sequence with two semisteps: Heating then Idle (or: On then Off); 
 100% heating means that the heater is On for the whole StepDuration, while 0% means Idle for the whole StepDuration 
If the time remaining for the next swich (On->Off or Off->On) is less than 0.5s, the step function returns, assuming that the next call will come in time with respect for the requested ration
Note that the first Heat is long for kStepDuration
*  |..on..|....Idle....|
*  |<--kStepDuration-->| 
*/
//#define kStepDuration       10000 // unsigned long; NOT UNDER 2000
//
//#define kFirstHeatDuration  15000 // unsigned long; ALWAYS UNDER kStepDuration because the On/StepDuration ratio is reproduced at all Step action
// TODO: check this

// TODO: librairie Timer_One : plus pr�cise ???

// TODO: essayer ResponsiveAnalogRead


/* *** Probes *** */



/* framework for PT100; 

 * this define is not fully used yet */
//#define USE_PT100 true
//#if USE_PT100
////const int pinForPT100 = 5;
//# define pinForPT100 5 // int
//#endif

/* *** Heater *** */
const int pinForHeater = 10;
/* *** ****** *** */


/* *** DISPLAY *** */
/* used as: LedControl(pinForMAX72XX[0], pinForMAX72XX[1], pinForMAX72XX[2], pinForMAX72XX[3]);
 * i.e.     LedControl(dataPin, clockPin, csPin, numDevices)
 *  connect DIN to your mosi_pin and CS to your set cs_pin
 *  numDevices: is the number of cascaded MAX72XX devices you're using with this LedControl. The library can address up to 8 devices from a single LedControl-variable.
 * more infos on: http://wayoda.github.io/LedControl/pages/software                                   */
const int pinForMAX72XX[4] = {11, 13, 10, 1}; // default from library example is (12,11,10,1); previous good setting: {11, 13, 10, 1}

//const int pinForMAX72XX[4] = {11, 9, D10, 1};
//const int pinForMAX72XX[4] = {PIN_SPI_MOSI,  9, PIN_SPI_SS, 1}; // 11, 9, 10, 1
//const int pinForMAX72XX[4] = {PIN_SPI_MOSI, 12, PIN_SPI_SS, 1}; 
//const int pinForMAX72XX[4] = {PIN_SPI_MOSI,  8, PIN_SPI_SS, 1}; 
//const int pinForMAX72XX[4] = {PIN_SPI_MOSI,  PIN_SPI_SCK, PIN_SPI_SS, 1};  // 11, 13, 10, 1

//const int pinForMAX72XX[4] = {PIN_SPI_MISO,  9, PIN_SPI_SS, 1}; // 12,  9, 10, 1
//const int pinForMAX72XX[4] = {PIN_SPI_MISO, 11, PIN_SPI_SS, 1}; // 12, 11, 10, 1
//const int pinForMAX72XX[4] = {PIN_SPI_MISO,  8, PIN_SPI_SS, 1}; // 12,  8, 10, 1
//const int pinForMAX72XX[4] = {PIN_SPI_MISO,  PIN_SPI_SCK, PIN_SPI_SS, 1}; // 12, 13, 10, 1

//const int pinForMAX72XX[4] = {9,  6, 5, 1};  // 9,  6,  5, 1
//const int pinForMAX72XX[4] = {A1, A2, A3};  // 15, 16, 17, 1

/* *** ******* *** */
