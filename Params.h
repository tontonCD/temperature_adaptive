// file: Param.h

#include <Arduino.h> // just for definition of "A5"


/* *** Modules *** */
/* framework for  Sensor:   DS18B20                      */
#define USE_DS18B20 false
/* framework for  Sensor:   PT100, with optional MCP342x */
#define USE_PT100   true
#define USE_MCP342x true // needs USE_PT100 to work
/*  framework for Display:  MAX7219 (7-Segment Display)  */
#define USE_MAX72XX false
/*  framework for Display:  SSD1306 (OLED 128x32)        */
#define USE_SSD1306 true

/* *** PARAMETERS *** */

/** a Step is a sequence with twho semisteps: Heating then Idle (or: On then Off); 
 100% heating means that the heater is On for the whole StepDuration, while 0% means Idle for the whole StepDuration 
If the time remaining for the next swich (On->Off or Off->On) is less than 0.5s, the step function returns, assuming that the next call will come in time with respect for the requested ration
Note that the first Heat is long for kStepDuration
*  |..on..|....Idle....|
*  |<--kStepDuration-->| 
*/
#define kStepDuration       10000 // unsigned long (ms); NOT UNDER 2000

#define kFirstHeatDuration  15000 // unsigned long (ms); ALWAYS UNDER kStepDuration because the On/StepDuration ratio is reproduced at all Step action

/* *** Wiring *** */
/* DS18B2:0 */
# define ONEWIRE_PIN    A0  // for DS18B20; (A0 is 14, 9 is D9, on Nano)
/* PT100   (the pin the PT100 is connected to, IF directly connected to a pin) */
extern const int pinForPT100;
/* MCP342x (the pin for the PT100 MCP342x, IF the pt100 is connected thru it)  */ 
# define SDA_PIN        A4  // send and receive data; default: A4 (GPIO 21 on ESP32, 18 on Nano); sometimes labeled as "SDI"
# define SCL_PIN        A5  // clock signal;          default: A5 (GPIO 22 on ESP32, 19 on Nano); sometimes labeled as "SDC" or "SCK"
// TODO: pinout.png says A4/A5 => 27/28, ???, no it's 18/19
/* MAX7219 7-Segment Display */
extern const int pinForMAX72XX[4]; // default from library is (12,11,10,1); set in Params.cpp (as: {11, 13, 10, 1})
/* *** Heater *** */
extern const int pinForHeater;
/* *** ****** *** */


/* *** DISPLAY *** */ // TEST MERGE
#define USE_MAX72XX false
extern const int pinForMAX72XX[4]; // default from library is (12,11,10,1)
/* *** ******* *** */
