// file: Temperature_Adapative.ino
// compiles for: Arduino Nano
// 
// DS18B20 uses: OneWire,   on pin:  ONEWIRE_PIN =      9             (D9 on board (Nano))
// PT100:        (ADC)      on pin:  Ax                               (if no MCP342x, just use any analog input pin)
// MCP342x uses: TwoWire,   on pins: SDA_PIN/SCL_PIN = 18, 19         (A4/A5 on board (UNO, Nano))
// MAX72XX uses: SPI        with LedControl(dataPin,clockPin,csPin,numDevices)  
//                                         on pins = 11, 13, 10, 1       
//      dataPin = DataIn        of the first MAX, 
//      clockPin= CLK-pin       of the first MAX7221
//      csPin   = LOAD(/CS)-pin of the first ,  pin for selecting the device
// (MOSI/CLK/CS + numDev)  (via LedControl)        device is 0x68
                
//               default for MOSI=D11, SCK=13, MISO=12(=CS?), SS=D10   
// issue :
//  MAX72XX can't work with both MCP342x and DS18B20; 
//  this on Arduino, but may change pins on other as 8266 (uncheck)
// issue2 : 
//  code may not compile if whole options, caused to not enough memory on board (2048 bytes)

#include "Params.h"

// I2C address for MCP342x: 0x68
// I2C address for SSD1306: 0x3D for 128x64, 0x3C for 128x32
// I2C address for GC9A01:  ??



// TODO: check ESP8266, ESP32, Arduino (Nano, ...)





#if USE_DS18B20
# include "DSProbes.h"
#endif

//#include "HeatControllerStep.h"
#include "HeatController.h"


/* wiring a PT100:
 *      + ----- Vcc
 *      |
 *    R=100
 *      |
 *      + ----- Ax   
 *      |         
 *    PT100
 *      |
 *      + ----- GND  
 *
 * *  for Ax, @see pinForPT100
 *
 * wiring a DS18B20:
 *      VDD ---+---- Vcc 
 *             |
 *           R=4.7k
 *             |
 *      DQ ----+---- Ax
 *        
 *      GND -------  GND
 *
 * *  for Ax, @see SCL_PIN in "Param.h"; default is A5
 * 
 * wiring PT100 (three-wires) and MCP3421 :
 *    PT100         MCP3421         Arduino
 *      R1 --------------- R=3.32k -- Vcc
 *      R2 -----------VIN+
 *      R3 ---------- VIN- 
 *                    VDD ----------- Vcc
 *                    GND ----------  GND
 *                    SDA ----------  A4 
 *                    SCL ----------  A5
 *      
 *      ? R=3.32k can be changed?
 *      ? pull up SCL & SDA, using 1.1k resistor to 5V?
 */



#if USE_MAX72XX // display device.
#include "LedControl.h" // doc: http://wayoda.github.io/LedControl/pages/software
LedControl lc = LedControl(pinForMAX72XX[0], pinForMAX72XX[1], pinForMAX72XX[2], pinForMAX72XX[3]); // i.e. (11,13,10,1);
/*
void printNumber(int v); 
*/
void printNumber(float value, int displayPart); // parameter: "displayPart" is for selecting the 4-digit segment, 1 (at left) or zero (at right)
#endif

#if USE_SSD1306 // display device.

# if false
// use adafruit: too heavy, the object dosn't initialise
//#include <SPI.h> // not usefull
//#include <Wire.h> // not usefull
#include <Adafruit_GFX.h> // usefull?

#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH    128 // OLED mySSD1306 width, in pixels
#define SCREEN_HEIGHT    32 // OLED mySSD1306 height, in pixels
#define OLED_RESET        4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C // <- See datasheet for Address: 0x3D for 128x64, 0x3C for 128x32

Adafruit_SSD1306 mySSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
# else

// use "lcdgfx by Alexey Dynda", https://github.com/lexus2k/ssd1306 (or ssd1306)
#  include "lcdgfx.h"
DisplaySSD1306_128x64_I2C ssd1306Display(-1);

void printNumber(float value);
# endif 
#endif // end: USE_SSD1306


unsigned long currentTime = 0;    // the current Time        when calling checkStepV3
float         currentTemp = 0;    // the current Temperature when calling checkStepV3
float         alternateTemp = 0;  // 2nd probe, not used for computations // renamed from: currentTemp2


/* ** config (displaced) ** */
///const int pinForProbes = 9;
///const int pinForHeater = 10;



/* params */
// UNUSED, // TODO: shoud?
///unsigned long delay_None2On = 2000;        // 2s; time from start where no action
///unsigned long delay_OnToOffgrowing = 1000; //  1s; max time in the sOn state;



/* setting: the temperature we are expected to reach */
float expectedTemp = 40.00;


/* ** internal ** */
#if USE_DS18B20
DsProbes dsProbes;
#endif

#if USE_PT100
#include "WMPT100.h"
WMPT100_ *myPT100; // instanciated later, in setup(): myPT100 = WMPT100_::create();
#endif

HeatController heatController;

void setup() {
  
    //Serial.begin(57600); // 9600, 74880
    Serial.begin(9600); // 9600 better on Nano
 
    // for Leonardo: wait until serial is ready
    while (!Serial) 
        delay(10);
    
    // start message
    Serial.println();
    Serial.println("***********************************");
    Serial.println("Project: Temperature_Adaptative.ino");

    Serial.println("Params:");
#if USE_DS18B20
    Serial.print(  " DS18B20:     true, on pin ONEWIRE_PIN: "); Serial.println(ONEWIRE_PIN);
#else
    Serial.println(" DS18B20:     false");
#endif
#if USE_PT100
    Serial.println(" PT100:       true");
#else
    Serial.println(" PT100:       false");
#endif
#if USE_MCP342x
    Serial.print(  " USE_MCP342x: true, on pins SDA_PIN, SCL_PIN : "); Serial.print(SDA_PIN); Serial.print(", "); Serial.println(SCL_PIN);
    // 18, 19
    // TODO: @see https://webge.github.io/MCP342x/ for MCP3422, MCP3423 et MCP3424 
#else
    Serial.println(" USE_MCP342x: false");
#endif
#if USE_MAX72XX
    Serial.print(  " USE_MAX72XX: true, on pins: "); 
    Serial.print(pinForMAX72XX[0]); Serial.print(", ");  Serial.print(  pinForMAX72XX[1]); Serial.print(", "); 
    Serial.print(pinForMAX72XX[2]); Serial.print(", ");  Serial.println(pinForMAX72XX[3]); //{11, 13, 10, 1};);
#else
    Serial.println(" USE_MAX72XX: false");
#endif

#if USE_SSD1306
    Serial.print(" USE_SSD1306: true, on pins SDA_PIN, SCL_PIN : "); Serial.print(SDA_PIN); Serial.print(", "); Serial.println(SCL_PIN);

    /*
    // SSD1306_SWITCHCAPVCC = generate mySSD1306 voltage from 3.3V internally
    if(!mySSD1306.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
        Serial.println(F("SSD1306 allocation failed"));
        for(;;); // Don't proceed, loop forever
    }
    */
    ssd1306Display.begin();
    ssd1306Display.fill(0x00);
    
    ssd1306Display.setFixedFont(comic_sans_font24x32_123);
 
#else
    Serial.println(" USE_SSD1306: false");
#endif


    delay(1000);
    // TODO: something is written before, check why
    // -> a baud rate is used before we fix it in Serial.begin(74880), it depends on the Board used; seems 57600 for Arduino, 74880 for ESP, 


#if USE_DS18B20
    /* ** check probes ** */
    int found = dsProbes.scanDevices(ONEWIRE_PIN); // ONEWIRE_PIN VS SCL_PIN
 
    // log
    Serial.print("DS18B20, Found ");Serial.print(found);Serial.print(" probe");
    if(found>1)
        Serial.print("s");
    if(found>0)
    {
        Serial.print(" on pin: "); Serial.print(ONEWIRE_PIN); Serial.println(":");
        
        for(int i=0; i<found; i++)
        {
            // TODO: BUG here, check
///            Serial.print(" - "); Serial.println(dsProbes.getProbeAddr(i));
        }
    } else {
        Serial.print(" on pin: ");Serial.println(ONEWIRE_PIN);
    }
 
#endif


#if USE_PT100
    myPT100 = WMPT100_::create();
    myPT100->init(); 
#endif




  heatController.setTempTarget(expectedTemp, pinForHeater);
  // TODO: param stepDuration => 1000ms
  
  /* 
   * Vx : steps can return while heater is On; be carefull
   */




#if USE_MAX72XX
    /* wakeup call                            */
    lc.shutdown(0,false);
    /* Set the brightness to a medium values  */
    lc.setIntensity(0,8);
    /* and clear the display                  */
    lc.clearDisplay(0);
#endif

#if USE_SSD1306
    /*
    // Show initial mySSD1306 buffer contents on the screen --
    // the library initializes this with an Adafruit splash screen.
    mySSD1306.display();
    delay(2000); // Pause for 2 seconds
    
    // Clear the buffer
    mySSD1306.clearDisplay();
    */
#endif 

  delay(2000);


  /* log start time */
  unsigned long startTime = millis();
  Serial.print("Starting at ");
  Serial.print(startTime);
  Serial.println("ms");

  // assume that config is all-done:
  /* HCGlobalState newState = */ heatController.setReady();
} // end: setup()


// ssd1306
int xHours =    6;
int xSeconds =  xHours +   44; // +48 => 54;
//int xMinutes =  xSeconds + 24; // 78;
int xMinutes =  xSeconds + 12; // ;
int xEMinutes = xMinutes + 24; // 126;


void loop() {

    // tell that we are start the loop()
    Serial.println("----------");

    /* ** COMMANDS ** */
    // CHECK if a command is available from Serial (for changing parameters, e.g. the target temperature) ** */
    if (Serial.available()) {
        int inByte = Serial.read();
        float value = float(inByte - '0');
        //Serial.print("** new Temp value,  "); Serial.println(value);
        while( Serial.available() )
        {
            inByte = Serial.read() - '0' ;
            //Serial.print("** new Temp value: "); Serial.print(value); Serial.print("*10 + "); Serial.print(inByte); 
            
            value = 10 * value + float(inByte);
            //Serial.print(" = ");Serial.println(value);
        }
      Serial.print("** new Temp value: "); Serial.println(value); //Serial.print(":\t");
      expectedTemp = value;
      heatController.setTempTarget(expectedTemp, pinForHeater);
    }

    // get Time
    currentTime = millis();

    /* ** GET SENSORS: ** */
#if USE_DS18B20
  currentTemp = dsProbes.getProbeTemp(0);
  alternateTemp = dsProbes.getProbeTemp(1);

  Serial.print(" Temperature as DS18B20 (1): "); Serial.print(currentTemp  ); Serial.println("°C");
  Serial.print(" Temperature as DS18B20 (2): "); Serial.print(alternateTemp); Serial.println("°C");
#endif

#if USE_PT100

# if USE_MCP342x ==  false

/*
 *  I1 = I2, and because I=U/R:
 *   =>  volts/(RX+resPT) = (V-volts)/resSerie
 *   =>  volts*resSerie = (V-volts)*(RX+resPT)
 *   =>  (RX+resPT) = volts*resSerie / (V-volts)
 *   =>  resPT = volts*resSerie / (V-volts) - RX
 */


    float resPt = myPT100->readOhms();
    currentTemp = WMPT100_::ohmsToTemp(resPt);

# else // with MPC

    //uint8_t err = myMCP.convertAndRead();
    float resPt = myPT100->readOhms();
    currentTemp = WMPT100_::ohmsToTemp(resPt);
    
# endif

    Serial.print(" Temperature as PT100: "); Serial.print(resPt); Serial.print(" Ohms => "); Serial.print(currentTemp); Serial.println("°C");

#endif // USE_PT100



    /* ** DISPLAY: ** */
#if USE_MAX72XX
    printNumber(currentTemp,   0);
    printNumber(alternateTemp, 1);
#endif
#if USE_SSD1306
    printNumber(currentTemp);
#endif 


    /* ** PROCESS: ** */
    heatController.checkStepV3(currentTemp, currentTime); // could write: HCGlobalState newState = ...
    
} // end: loop()

/*
float resToTemp(float resPt)    {
    
    double resPt_0 = 100.0;     // the PTxxx reference resistance, that is, at 0° (100 ohms for the PT100)

    // T = ( -A + sqrt(A*A - 4*B * (1 - Rt/R0)) ) / (2*B)
    float coeffA =   0.0039083;
    float coeffA2 =  coeffA*coeffA; // A*A
    float coeffB =   0.0000005775;
    
    float result = ( -coeffA + sqrt(coeffA2 - 4*coeffB * (1 - resPt/resPt_0)) ) / (2*coeffB);
    Serial.print("Temp from PT100: "); Serial.println(result);

    return result;
}
*/

/* *** DISPLAY *** */
/* UTILS */
#if USE_MAX72XX || USE_SSD1306
/** converts a float to ints, e.g. 12.34 => [1, 2, 3, 4], with two-digits left, two-digits right; 
  padding (left or right) is applied if requiered */
void buildValuePart(float value, int valueInts[5]) {

    // represent value as successive digits 
    int iHundreds, iTens, iOnes, fTens, fOnes;  // note: hundreds is not handled because MAX72XX cant't print it (could be)
    boolean negative=false;                     // note: sign ('-') is not handled

    //int valueIntssz[5] = {0, 0, 0, 0, 0};
    valueInts[0] = valueInts[1] = valueInts[2] = valueInts[3] = valueInts[4] = 0;
    char valueString[6] = "00.00";

    if(value < -999 || value > 999)  
        return; // ??? 
    if(value<0) {  
        negative = true; 
        value = -value; // was: = value*-1;  
    }

    int intPart = int(value);
    int fractPart = (int(value*100)) % 100;
// TEST
#if false
    Serial.print("temp (float): "); Serial.print(value);
    Serial.print(" => \'"); Serial.print(intPart); Serial.print(" . "); Serial.print(fractPart); Serial.println("\'"); 
#endif
    iOnes = intPart%10;  
    intPart = intPart/10;  
    iTens = intPart%10;  
    intPart = intPart/10;  
    iHundreds = value;  
    
    valueInts[0] = iHundreds;
    valueInts[1] = iTens;
    valueInts[2] = iOnes;

    fOnes = fractPart%10;  
    fTens = fractPart/10;  
    valueInts[3] = fTens;
    valueInts[4] = fOnes;
}
/** converts a float to chars, e.g. 12.34 => "12.34", with two-digits left, two-digits right; 
  padding (left or right) is applied if requiered */
void buildValuePart(float value, char valueString[6]) {
    int valueInts[5];
    
    buildValuePart(value, valueInts);

    //char valueString[6] = "00.00";

    valueString[0] = valueInts[1] + '0'; // we don't use hundreds
    valueString[1] = valueInts[2] + '0';
    // (valueString[2]: dot here)
    valueString[3] = valueInts[3] + '0';
    valueString[4] = valueInts[4] + '0';
}

#endif

#if USE_MAX72XX
/*
void printNumber(int v) {  
    int ones;  
    int tens;  
    int hundreds; 

    boolean negative=false;

    if(v < -999 || v > 999)  
        return;  
    if(v<0) {  
        negative=true; 
        v=v*-1;  
    }
    ones = v%10;  
    v =    v/10;  
    tens = v%10;  
    v =    v/10; 
    hundreds = v;  
    if(negative) {  
        //print character '-' in the leftmost column  
        lc.setChar(0, 3, '-',false);  
    } 
    else {
        //print a blank in the sign column  
        lc.setChar(0, 3, ' ',false);  
    }  
    //Now print the number digit by digit 
    lc.setDigit(0, 2, (byte)hundreds,false);
    lc.setDigit(0, 1, (byte)tens,    false); 
    lc.setDigit(0, 0, (byte)ones,    false); 
} */
void printNumber(float value, int displayPart) {

#if true
// TODO: this part (for MAX72XX) was factorized for SSD1306 => apply here too

    int iHundreds, iTens, iOnes, fTens, fOnes;  // note: hundreds is not print if using MAX72XX (could be)
    boolean negative=false;                     // note: sign ('-') is not print

    // replace { ones, itens, ('.', ), 
    int valueInts[5]; // = [0, 0, 0, 0, 0];
    char valueString[6] = "00.00";

    if(value < -999 || value > 999)  
        return; // ??? 
    if(value<0) {  
        negative = true; 
        value = -value; // was: = value*-1;  
    }

    int intPart = int(value);
    int fractPart = (int(value*100)) % 100;
// TEST
# if false
    Serial.print("temp (float): "); Serial.print(value);
    Serial.print(" => \'"); Serial.print(intPart); Serial.print(" . "); Serial.print(fractPart); Serial.println("\'"); 
# endif
    iOnes = intPart%10;  
    intPart = intPart/10;  
    iTens = intPart%10;  
    intPart = intPart/10;  
    iHundreds = value;  
    
    valueInts[0] = iHundreds;
    valueInts[1] = iTens;
    valueInts[2] = iOnes;
#endif

    // select displayPart (as 2x 4-digit parts)
    int numDigit = 3;   // index for the first digit to set from left to right (default) => (3, 2, 1, 0)
    if(displayPart==1)
        numDigit = 7;   // => (7, 6, 5, 4)
    
    // make an animation, first, erase the fourth digits
    lc.setChar(0, numDigit,  ' ', false); // void setChar(int addr, int digit, char value, boolean dp);
    lc.setChar(0, numDigit-1,' ', false);
    lc.setChar(0, numDigit-2,' ', false);
    lc.setChar(0, numDigit-3,' ', false);
    delay(100); 
    
    /* wrong... TODO ?
    if(negative) {  
        //print character '-' in the leftmost column  
        lc.setChar(0, 5, '-', false);  
    } 
    else {
        //print a blank in the sign column  
        lc.setChar(0, 5, ' ', false);  
    }  
    */
    
    // Now print the number digit by digit, as "xx.xx"
    
    //lc.setDigit(0, 4,(byte)hundreds, false); TODO: display "xxx.x" if > 100
    delay(100); lc.setDigit(0, numDigit,  (byte)iTens,     false); 
    delay(100); lc.setDigit(0, numDigit-1,(byte)iOnes,     false); 
    delay(100); lc.setDigit(0, numDigit-1,(byte)iOnes,     true); // for the dot
    /* setDigit: "There are only a few characters that make sense here :"
     *   '0','1','2','3','4','5','6','7','8','9','0',
     *  'A','b','c','d','E','F','H','L','P',
     *  '.','-','_',' ' 
     */

    fOnes = fractPart%10;  
    fTens = fractPart/10;  
    valueInts[3] = fTens;
    valueInts[4] = fOnes;

    
    //Now print the number digit by digit 
    delay(100); lc.setDigit(0, numDigit-2,(byte)fTens,   false);
    delay(100); lc.setDigit(0, numDigit-3,(byte)fOnes,   false); 
} 

#endif // end: #if USE_MAX72XX

#if USE_SSD1306
void printNumber(float value) {
    // clean:
    ssd1306Display.fill(0x00);
    
    char valueString[6] = "00.00";
    buildValuePart(value, valueString);
    
    //ssd1306Display.setFixedFont(ssd1306xled_font8x16);  // OK for title
    NanoFont temp = NanoFont(ssd1306xled_font8x16);
    ssd1306Display.setFont( temp );
    ssd1306Display.printFixed(10, 0, "Temperature:"); // printFixed ( lcdint_t  xpos, lcdint_t  y, const char *  ch, EFontStyle  style = STYLE_NORMAL)   
  
    ssd1306Display.setFixedFont(courier_new_font11x16_digits);  // OK for value
    ssd1306Display.printFixedN(10, 24, valueString, STYLE_NORMAL, 1); 
}
#endif 
/* *** ******* *** */
