// file: WMMCP342x.cpp

#include "Params.h"
#if USE_MCP342x



#include "WMMCP342x.h"
#include <Wire.h>

///#include <Wire.h>
// use our own "Wire" variable
//TwoWire mpcWire = TwoWire(); OLD
//TwoWire *mpcWire; DISPLACED

WMMCP342x::WMMCP342x(void) {
    mpcWire = new TwoWire();
    
    // address: the 7-bit slave address (optional); if not specified, join the bus as a master. 
    mpcWire->begin(SCL_PIN); // TODO: check if no conflict with OneWire
 //Wire.begin(SCL_PIN);
}
void WMMCP342x::mcpReset() {
    // Reset devices
    MCP342x::generalCallReset();
    delay(1); // MC342x needs 300us to settle, wait 1ms
}
bool WMMCP342x::mcpCheck() {
    // request 1 byte on mAdress:
 
    mpcWire->requestFrom(mAddress, (uint8_t)1); // instead of: Wire.requestFrom(mAddress, (uint8_t)1);
    if (!mpcWire->available()) {
    //Wire.requestFrom(mAddress, (uint8_t)1); // instead of: Wire.requestFrom(mAddress, (uint8_t)1);
    //if (!Wire.available()) {
        Serial.print(" MCP342x: No device found at address "); Serial.println(mAddress, HEX);
        return false;
    }
    Serial.print(" MCP342x: Device found at address 0x"); Serial.println(mAddress, HEX);

    return true;
}

float WMMCP342x::convertAndRead()  {
    long result_raw = 0;
    MCP342x::Config theConfig;

    // Initiate a conversion; convertAndRead() will wait until it can be read
    uint8_t err = mAdc.convertAndRead(
        MCP342x::channel1, 
        MCP342x::oneShot,
        mResolution, MCP342x::gain1,
        1000000, 
        result_raw, 
        theConfig);
    // my measures: 2442 if resolution16, and 9768 if resolution18
  
    if (err) {
        Serial.print(" convertAndRead: ERROR");
        Serial.println(err);

        return -1.0; // error
    }
    

    Serial.print(" ADC Value: "); Serial.print(result_raw);
   
        // test
    long result = result_raw;
    MCP342x::normalise(result, theConfig);
    Serial.print("(");  Serial.print(result_raw); Serial.print(" => (normalized): "); Serial.print(result);
   /* my measures, resolution16:
    *  Result: 2443 => (normalized) 78176
    *  Result: 2442 => (normalized) 78144
    * my measures,resolution18:
    *  Result: 9771 => (normalized) 78168
    *  Result: 9770 => (normalized) 78160
    *  Result: 9772 => (normalized) 78176
    */
    
    // try to convert to Volts // TODO: instead use result and (4096.0 * 64)
    //float Vin_raw = -1; // or: = mVRef * float(result_raw); for debug;
/*        switch (int(theConfig.getResolution())) {
            case 12:
                mDivisor = 4096.0;   break;
            case 14:
                mDivisor = 4096.0 *  4;   break; 
            case 16:
                mDivisor = 4096.0 * 16;   break; 
            case 18:
                mDivisor = 4096.0 * 64;   break; 
        }
        */
    /*float VinN1 = mVRef * float(result_raw) / mDivisor; // expecting: 0.151 (measured)*/
    float Vin =  mVRef * float(result) / (4096.0 * 64.0 * 8.0); // 8.0 because 18 to 21 bits
    // expected (mesured): 0.115
    // FALSE, the ADC use its own ref = 2.048
    float divisor = (4096.0 * 64.0 * 8.0 / 2); // 8.0 because 18 to 21 bits, 2.0 because signed
    float VrefADC = 2.048;
    Vin = float(result)*VrefADC/divisor;
        
//    Serial.print(" (=> "); Serial.print(Vin); Serial.print(" / "); Serial.print(mDivisor); Serial.print(" )");
    Serial.print(" => "); Serial.print(Vin); /*Serial.print(" / "); Serial.print(VinN1);*/ Serial.print(" V");
    // RESULT: 0.18V (float, 18bits) OK! (expecting 0.153)

//#if false
    // 2-wires: Rrtd = (Vin * mRRef) / (mVRef - Vin)
    float Rrtd2 = (Vin * mRRef) / (mVRef - Vin);
    Serial.print(" => (2: "); Serial.print(Rrtd2);Serial.print(" Ohms)");
    
    // RESULT: 125.54 OK! (expecting 100+)
    // prints: "Result: 9772 => (normalized) 78176 (=> 46006.58) (=> 46006.58 / 262144.00 ) => 0.18V => 125.57"
    // 125.64 => 60.6°
//#else
    // 3-wires: Rrtd = mRRef * (2*mVRef - Vin)/(mVRef - Vin)
    float Rrtd3 = mRRef * (Vin+Vin - mVx)/(mVRef - mVx);
    // should be : 3300 * (0,151 + 0,151 - 0,153) / (4,691 - 0,151) = 108.35
    // or:         3300 * (0,170 + 0,170 - 0,153) / (4,691 - 0,151) = 135.98
    Serial.print(" => (3: "); Serial.print(Rrtd3);Serial.print(" Ohms)");
    // 5V, prints: "Result: 9595 => 0.17V => (2: 123.21) => (3: 137.43)"
    // 137.43 => 90.7°
    //         "Result: 9611 => 0.29V => (2: 216.29) => (3: 311.16)"
    // 3V, prints: Result: 7409(7409 => (normalized): 59272 ) => 0.10V => (2: 94.32) => (3: 80.50)



    // $$R_{RTD}  = \frac{ V_{In} * R_{REF} }{ V_{REF} - V_{In} } - 2 * R_1$$
    float Rrtd3b = (Vin * mRRef) / (mVRef - Vin);// - 2 * 0.5;
    Serial.print(" => (3b: "); Serial.print(Rrtd3b);Serial.print(" Ohms)");
    // "ADC Value: 7404(7404 => (normalized): 59232 => 0.12 V => (2: 108.67 Ohms) => (3: 108.39 Ohms) => (3b: 107.67 Ohms)"
    // OK,  20° is  107.79
    //      19      107.40
    //      18      107.02
    //      17      106.63


    // Rp = Rw * (Vref - 2 * Vin)/(Vin - Vref) - Vin *Rref/(Vin - Vref)
    float Rrtd3c = 0.5 * (mVRef - 2 * Vin)/(Vin - mVRef) - Vin *mRRef/(Vin - mVRef);
    Serial.print(" => (3c: "); Serial.print(Rrtd3c);Serial.print(" Ohms)");

    // more Trace:
    //Serial.print(" mRRef * (2*Vin - mVx)/(mVRef - mVx) = ");
    //Serial.print(mRRef); Serial.print(" * ("); Serial.print(Vin); Serial.print("+");Serial.print(Vin); Serial.print(" - ");Serial.print(mVx);Serial.print(")/(");Serial.print(mVRef);Serial.print(" - ");Serial.print(mVx);Serial.print(")");
        
//#endif
    
        Serial.println();
    
    
    //return err;
    return Rrtd3c;
    
 } // convertAndRead


#endif // #if USE_MCP342x
