// file: WMMCP342x.h

#include "Params.h"
#if USE_MCP342x


// dependancies
#include "Arduino.h"
#include <MCP342x.h>
#include <Wire.h>


#ifndef WMMCP342x_h
#define WMMCP342x_h


class WMMCP342x  {
private:
    TwoWire *mpcWire;
    
public:
  /* PARAMS (2-Wires) */
    float mRRef = 3243.0;   // Ohms (not KOhm)

    /* Vref:
     *  5V => 4.611 - 4.622
     *  3V => 3.567 - 3.569
     */
    float mVRef = 3.568;    // for me, 5V=>4.691, 3V=>3.568
    
    // 3-Wires
    float mVx =   0.116;    // for me, 5V=>0.151, 3V=>0.116
    
    // we mesured also: Vin = 0.153
//    float mDivisor = -1; // for debug

    MCP342x::Resolution mResolution = MCP342x::resolution18; // 0x084 for 18bits; 0x0C for 18bits

    uint8_t mAddress = 0x68; // 0x68 is the default address for all MCP342x devices
    MCP342x mAdc = MCP342x(mAddress);

    WMMCP342x(void);
    
    void mcpReset();
    bool mcpCheck();

    float WMMCP342x::convertAndRead(); // was: returned uint8_t as an Error
    
private:
    MCP342x::Config theConfig;
};


#endif // WMMCP342x_h
#endif // #if USE_MCP342x
