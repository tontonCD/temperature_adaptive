// file: WMPT100.cpp

#include "Params.h"

#if USE_PT100


#include "WMPT100.h"

WMPT100_ *WMPT100_::create()	{

#if USE_MCP342x==false
    Serial.println("Building a WMPT100_pin instance");
	WMPT100_pin *result = new WMPT100_pin();
    return result;
#else
	Serial.println("Building a WMPT100_mcp instance");
	WMPT100_mcp *result =  new WMPT100_mcp();
    ///if(result->mcpMCP==0)
    ///    Serial.println(" ERROR: mcpMCP==0");
    return result;
#endif
}


void WMPT100_::init()	{
    Serial.println(" init(), ERROR:     can't call directly the WMPT100_ class, use ::init() from WMPT100_pin or WMPT100_mcp");
}

float WMPT100_::readOhms()	{
    Serial.println(" readOhms(), ERROR: can't call directly the WMPT100_ class, use readOhms()  from WMPT100_pin or WMPT100_mcp");
	return -2.0; // error, don't use this class
}

float WMPT100_::ohmsToTemp(float resPt)    {
    
    double resPt_0 = 100.0;     // the PTxxx reference resistance, that is, at 0� (100 ohms for the PT100)

     // T = ( -A + sqrt(A*A - 4*B * (1 - Rt/R0)) ) / (2*B)
    float coeffA =   0.0039083;
    float coeffA2 =  coeffA*coeffA; // A*A
    float coeffB =   0.0000005775;
    
    float result = ( -coeffA + sqrt(coeffA2 - 4*coeffB * (1 - resPt/resPt_0)) ) / (2*coeffB);
    
    // trace (useless):
    // Serial.print(" Temp from PT100: "); Serial.println(result);

    return result;
}

/* *** with pin *** */

WMPT100_pin::WMPT100_pin()	{
}

void WMPT100_pin::init()	{
	pinMode(pinForPT100, INPUT);
}

float WMPT100_pin::readOhms()	{
	
	////currentTemp = dsProbes.getProbeTemp(0);
    int rawValue = analogRead(pinForPT100); // note: getProbeTemp() can be used if refactoring
/*
 *       +-- R1 -- resPt -- R2 --+-- resSerie --+
 *       |                       |              |
 *      GND                    pin8            Vcc      
 */
    
    // the rawValue is from 0 to 1024 (resolution for 10-bits), convert it to Volts:
    float volts = (float)rawValue * 5.0 / 1024.0; // default resolution = 10 bits = 1024

    Serial.print(" PT100 - raw value: "); Serial.print(rawValue);
    Serial.print(" -> volts: ");Serial.print(volts);

    if(rawValue==1023) {
        Serial.println(" -> error (wrong wiring ?)");
        return -1.0;
    }
    
    float  vcc = 5.0;           // Vcc (5.0 or 3.3)
    float  RX = 30.0;           // the resistance for the two PTxxx wires, R1+R2
    double resSerie = 500.0;    // the resistance added between the Arduino Pin and Vcc, for current limitation
    float  resPt;               // the PTxxx resistance in actual situation, to determine
    double resPt_0 = 100.0;     // the PTxxx reference resistance, that is, at 0� (100 ohms for the PT100)
    
    resPt = volts*resSerie / (vcc-volts);
    resPt = resPt - RX;
	
	return resPt; 
}
	
	
/* *** with MCP *** */
#if USE_MCP342x

//WMPT100_mcp::WMPT100_mcp()	{
//}

WMPT100_mcp::WMPT100_mcp()	{
    Serial.println(" Calling: WMPT100_mcp::WMPT100_mcp()");
	mcpMCP = new WMMCP342x();
}
WMPT100_mcp::~WMPT100_mcp()	{
	delete mcpMCP;
}
	
	
void WMPT100_mcp::init()	{
	mcpMCP->mcpReset();
      
    if(! mcpMCP->mcpCheck() ) {
        while (1)
            ;
	}
}

float WMPT100_mcp::readOhms()	{
	return mcpMCP->convertAndRead();
}
#endif // #if USE_MCP342x


#endif // #if USE_PT100
