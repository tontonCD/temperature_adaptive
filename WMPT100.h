// file: WMPT100.h

//#include <Arduino.h> // just for definition of A5

#include "Params.h"

#if USE_MCP342x
# include "WMMCP342x.h"
//WMMCP342x mcpMCP = WMMCP342x();
#endif


/** abstract class */
class WMPT100_	{

public:	
	virtual void init();
	virtual float readOhms();
	
	/** factory, returns a WMPT100_pin* or WMPT100_mcp* instance, but you can declare it as a WMPT100_* */
    static WMPT100_ *create();
	
	static float ohmsToTemp(float resPt);

//protected: // visible from WMPT100_pin or WMPT100_mcp
//    WMMCP342x *mcpMCP; // = WMMCP342x();

};


/** class for PT100, when directly connected to a pin */
class WMPT100_pin: public WMPT100_	{
public:	
	WMPT100_pin();

	void init();
	float readOhms();
	
};

#if USE_MCP342x
/** class for PT100, when connected thru an MCP342x */
class WMPT100_mcp: public WMPT100_	{
public:	
	WMPT100_mcp();
	~WMPT100_mcp();

	void init();
	float readOhms();
	

protected: // visible from WMPT100_
	WMMCP342x *mcpMCP; // = WMMCP342x();
};
#endif // #if USE_MCP342x
