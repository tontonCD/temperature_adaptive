[English Version](ReadMe.md).  
Project URL: [https://gitlab.com/tontonCD/temperature_adaptive]()

# Temperature-Adaptative

## 

## Le Projet :

Temperature_Adaptative est une application pour cartes Arduino, de **contrôle de température**, ce genre d'application est généralement connue sous le nom de PID.

Celle-ci a été conçu à l'occasion d'un projet de **brassage**, pour automatiser le maintient des paliers de température.

Dans le principe la carte est reliée :

- à une sonde de température,

- à un relai, sur lequel est branché un élément chauffant (résistance),

<u>Le challenge</u> : l'application ne connait pas la réaction du système (puissance de la résistance, quantité d'eau, température de la pièce, ...) et est sensée mesurer sa réactivité, c'est à dire quelle puissance il faut déployer pour atteindre la température à atteindre.

## Caractéristiques :

<u>Principe</u> : la résistance est activée sur une certaine durée (définie en paramètre), puis observe quelle température maximum est atteinte, ce qui donne aussi pendant combien de temps une action de chauffage a de l'effet. On en déduit un **ratio** degrés/seconde.

Actuellement uniquement pour sondes **DS18S20**. 

Les sondes **PT100** sont désormais implémentées, il est recommandé d'utiliser un ADC pour plus de précision, actuellement un **MCP342x**. 

Des schémas de câblages se trouvent dans le fichier DSProbe.h

- le contrôle de température est pseudo-multitâche, c'est à dire que la fonction rend la main en moins d'une seconde tout en traçant le % d'énergie donné à la résistance,
- le system envoie des information (date, température, statut) par le port série, que l'on peut afficher afficher dans le "Moniteur série" de l'application Arduino,
- le "Moniteur série" permet également de saisir une nouvelle température cible, sur 2 chiffres,
- fonctions documentées dans le code, style JavaDoc,

Un affichage est géré, initialement un **MAX72XX** (afficheur 7-segments), et plus récemment un **SSD1306** (OLED).

Tout n'est pas à utiliser, il suffit d'une sonde, l'affichage est optionnel. D'ailleurs ***certains éléments sont incompatibles*** (SPI vs TwoWire vs OneWire, voir le code pour les précisions). 
Le relai peut même être omis si on désire simplement mesurer la température.

## Personnalisation

Le code est orienté programmation Objet. Il est en principe simple de le personnaliser, mais a encore besoin d'être affiné.

Utilisez le fichier "Param.h" (actuellement peu complet)

## Composants :

- UC: 
  Arduino (e.g. Nano), or ESP(1)

- probes:
  
  name | communication | Arduino pin
  
  --| -- |--
  PT100 | internal ADC | any Analog Pin
  DS18B20 | OneWire | any Analog Pin
  MCP342x(1) | TwoWire | SDA, SCL

- display:
  
  name | communication | Arduino pin
  
  --| -- |--
  
  MAX7219 | SPI | MOSI, SS, SCK
  TTGO T-Display | (embed) | 
  SD1306 | | (uncheck)
  
  (1) il semble qu'on ne peut faire fonctionner simultanément des périphériques de type OneWire ***ET*** TwoWire ; dans le cas où on le voudrait vraiment,
  une carte ESP serait à tenter car le framework permet de changer les pin pour les fonctions SDA et SCL (voir sdaPin/sclPin) !
  (2) en choisir une

**Note, Alimentation du PT100** : faire attention, si la carte est alimentée en 5V (par USB) le 5V en sortie du régulateur intégré n'est pas du tout garanti. En fait il est légèrement inférieur, (4.547 mesuré)[1: la doc technique parle d'un "dropdown"" de 1.1v] mais surtout il n'est pas stable.

Améliorations prévues
=

- (perso: donner des exemples du log, des copies d'écran, un schéma UML),
- déplacer plus de variables vers le fichier de paramètres,
- la température devrait être réglée par potentiomètre (type incrémental),
- prévoir de rédiger un schéma pour le câblage (mais on en trouve une description dans le code),
- régler une durée de palier (potentiomètre) et déclencher un signal sonore quand atteint ?

Versions:
=

3.3:

- rajout du MCP342x (ADC) for the PT100 probe, fonctionnel,

- rajout du SD1306 (display), fonctionnel,

3.2 : 

- Création du fichier Param.h
- Possibilité d'utiliser un HeatingDuration supérieur à StepDuration (le chauffage ne s'arrête pas en retournant à la boucle principale, permettant de lire une nouvelle valeur de température et l'afficher - prévoir doc)
- Meilleure gestion du HeatingTime (le temps de calcul entre deux Steps peut être supérieur à 1s, ce qui rendait tous les temps supérieurs à cette valeur); à améliorer pour les durées très petites.

3.1 : la classe HeatControllerStep peut rendre la main alors que le chauffage est actif; 
ceci n'était pas autorisé dans les version précédentes pour des raison de sécurité,
