[Version Française](ReadMe.FR.md).  
Project URL: [https://gitlab.com/tontonCD/temperature_adaptive]()

# Temperature-Adaptative

## The Project

Temperature_Adaptative is for Arduino boards, for **temperature control**; this kind of application is often known as PID. 

This one was intended because of a brewing project, to obtain an automatic temperature stabilization.

The project relies on:

- a temperature probe input,

- a relay, on which a heating component (resistor) will be plugged,

<u>The challenge</u> : the application doesn't know anything of the system composition (resistor power, water quantity, room temperature, ...) and is intended to measure its reactivity, i.e. how much power to deploy for the target temperature.

## Features :

<u>Principe</u> : the resistor is On at start for a time (defined as a parameter), then observe the maximum temperature that will be reached, and know also how many time a heat action would have an effect. We then obtain a **ration** as degrees/seconds.

Actually works only for **DS18S20** probes.

The **PT100** probe is now implemented and is recommended to be used with an ADC, actually an **MCP342x**.

You can find wiring schemas in the DSProbe.h file.

- the temperature is multitask-like, to say that the functions return almost immediately, keeping trace for the energy given to the resistor,
- the system sends information (time, temperature, status) that you can display within the "Serial Monitor" from the Arduino application,
- you can use the "Serial Monitor" for a new temperature target, use two-digits values,
- inline documentation in the JavaDoc style,

Display features are enabled, initially a **MAX72XX** (7 segments), et more recently an **SSD1306** (OLED).

All of these are not required for the whole to be functional, the probe is. In fact ***you can't*** combine some of them (SPI vs TwoWire vs OneWire, see the code for details).
The relay also can be omitted if you juste want to know about the temperature.

## Customization

The coding respects many of Object Oriented Programing. I hope it's easy to bring it modifications, but it needs to be refined first.

Please use the "Param.h" file (really basic for now)

## Components :

- UC: 
  Arduino (e.g. Nano), or ESP(1)

- probes:
  
  name | communication | Arduino pin
  
  --| -- |--
  PT100 | internal ADC | any Analog Pin
  DS18B20 | OneWire | any Analog Pin
  MCP342x(1) | TwoWire | SDA, SCL

- display:
  
  name | communication | Arduino pin
  
  --| -- |--
  
  MAX7219 | SPI | MOSI, SS, SCK
  TTGO T-Display | (embed) | 
  SD1306 | | (uncheck)
  
  (1) it seems that we can't have OneWire **AND** TwoWire working both together; in case of using both kind of devices,
  ESP may be the solution because we can change sdaPin/sclPin!
  (2) choose one
  
  

**Note, giving voltage for the PT100**: be aware, if the board is powered with 5V (via USB), finding 5V out of the embed regulator is not a guarantee. Really it is slightly less, (4.547 as measured[1: the datasheet talk about a "dropdown"" as 1.1v] but first of all it's not stable.



Planned features
=

- (personal: give examples from log, screenshots, UML scheme), 
- more usage of the parameters file, 
- the temperature target should be set from a potentiometer (incremental type); 
- build a wiring schema document (also you can fin some description in the code),
- a duration for a temperature setting should be settable (potentiometer), send a sound signal when reached?



Versions:
=

3.3: 

- added the MCP342x (ADC) for the PT100 probe, functional,

- added the SD1306 (display), functional,



3.2: 

- added the file Param.h
- possibility to set an HeatingDuration higher than the StepDuration (hitting doesn't stop when returning to the main loop, allowing temperature measuring/displaying),
- better handling the HeatingTime (the calculation time between two Steps can be higher than 1s, so all durations were higher); 

3.1: the HeatControllerStep class can return while the heating is On; 
this was forbidden in previous versions for security.
